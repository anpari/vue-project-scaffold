# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Components

- Vue 3
- Vite
- Tailwind
- Shadcn-Vue

## References

- [Tailwind install guide Vue3 + Vite](https://tailwindcss.com/docs/guides/vite#vue)
- [Vite project scaffolds](https://vitejs.dev/guide/#scaffolding-your-first-vite-project)
- [Shadcn-Vue setup guide](https://www.shadcn-vue.com/docs/installation/vite)